import type { ClientsConfig, ServiceContext, Cached } from '@vtex/api'
import { LRUCache, method, Service } from '@vtex/api'

const TIMEOUT_MS = 800

// Create a LRU memory cache for the Status client.
// The @vtex/api HttpClient respects Cache-Control headers and uses the provided cache.
const memoryCache = new LRUCache<string, Cached>({ max: 5000 })

metrics.trackCache('status', memoryCache)

// This is the configuration for clients available in `ctx.clients`.
const clients: ClientsConfig = {
  // We pass our custom implementation of the clients bag, containing the Status client.
  options: {
    // All IO Clients will be initialized with these options, unless otherwise specified.
    default: {
      retries: 2,
      timeout: TIMEOUT_MS,
    },
    // This key will be merged with the default options and add this cache to our Status client.
    serviceWorker: {
      memoryCache,
    },
  },
}

async function worker(ctx: ServiceContext, next: () => Promise<unknown>) {
  ctx.status = 200
  ctx.set('content-type', 'application/javascript; charset=utf-8')
  ctx.set('cache-control', 'public, max-age=7200')
  ctx.body = `importScripts('https://static.fanplayr.com/client/sw.js');`
  await next()
}

async function connect(ctx: ServiceContext, next: () => Promise<unknown>) {
  const cookieName = '_fphu'
  const { data } = ctx.request.query

  if (data) {
    ctx.set(
      'Set-Cookie',
      `${cookieName}=${data}; Path=/; Max-Age=315360000; HttpOnly; Secure; SameSite=Strict;`
    )
    ctx.body = data
  } else {
    const cookieValue = ctx.cookies.get(cookieName)

    if (cookieValue) {
      ctx.body = cookieValue
    } else {
      ctx.body = ''
    }
  }

  ctx.status = 200
  await next()
}

async function loader(ctx: ServiceContext, next: () => Promise<unknown>) {
  const {
    clients: { apps },
  } = ctx
  ctx.status = 302
  const appId = process.env.VTEX_APP_ID || ""
  const settings = await apps.getAppSettings(appId)
  if (settings.customLoader) {
    ctx.set('Location', settings.customLoader)
  } else {
    ctx.set('Location', 'https://cdn.fanplayr.com/client/production/loader.js')
  }
  await next()
}

async function fporderloader(ctx: ServiceContext, next: () => Promise<unknown>) {
  ctx.status = 302
  ctx.set('Location', 'https://cdn.fanplayr.com/cdp/fanplayr-latest.min.js')
  await next()
}

async function fplatest(ctx: ServiceContext, next: () => Promise<unknown>) {
  ctx.status = 302
  ctx.set('Location', 'https://cdn.fanplayr.com/cdp/fanplayr-latest.min.js')
  await next()
}

// Export a service that defines route handlers and client options.
export default new Service({
  clients,
  routes: {
    serviceWorker: method({
      GET: [worker],
    }),
    fpconnect: method({
      GET: [connect],
    }),
    fploader: method({
      GET: [loader],
    }),
    fporderloader: method({
      GET: [fporderloader],
    }),
    fplatest: method({
      GET: [fplatest]
    })
  },
})
