# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.2] - 2024-04-04

## [1.4.1] - 2023-05-17

## [1.4.0] - 2023-05-08

## [1.3.1] - 2022-10-18

## [1.3.0] - 2021-02-03

## [1.2.17] - 2021-02-02

Fix categories tracking

## [1.2.16] - 2021-02-02

Fix product id tracking

## [1.2.15] - 2021-01-20

## [1.2.14] - 2021-01-20

## [1.2.13] - 2021-01-19

## [1.2.12] - 2021-01-19

## [1.2.11] - 2021-01-19

## [1.2.10] - 2021-01-19

## [1.2.9] - 2021-01-19

## [1.2.8] - 2021-01-19

## [1.2.7] - 2021-01-19

## [1.2.6] - 2021-01-19

Fix cookie

## [1.2.5] - 2021-01-19

Fix route

## [1.2.4] - 2021-01-19

Disabled custom loader

## [1.2.3] - 2021-01-19

## [1.2.3] - 2021-01-19

## [1.2.2] - 2021-01-19

Changed from double to single qoute in head.html

## [1.2.1] - 2021-01-18

Fix config variable reading

## [1.2.0] - 2021-01-18

Implemented Enhanced User Identification
Mask plarform loader

## [1.1.0] - 2021-01-14

Implemented ```Enable Service Worker``` and ```Custom Loader``` settings.

## [1.0.3] - 2020-08-06

Tracking Fix
