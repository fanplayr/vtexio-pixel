import { canUseDOM } from 'vtex.render-runtime'

let currentPageData : FanplayrData;

import { Categories, Category, PixelMessage } from './typings/events'
import reinitialize from './modules/reinitialize';
import { FanplayrData } from './typings/fanplayr';

export default function() {
  return null
} // no-op for extension point

let toTrack = false;

export function targetingHandleEvents(e: PixelMessage) {
  switch (e.data.eventName) {
    case 'vtex:pageView': {
      toTrack = true;
      window.setTimeout(() => {
        if (toTrack) {
          reinitialize({
            pageType: "page",
            cartAction: "repeat"
          })
          toTrack = false;
        }
      }, 2000);
      return
    }
    case 'vtex:productView': {
      if (toTrack) {
        const { selectedSku, productName, productId, categoryTree } = e.data.product

        let price = 0;
        try {
          price = e.data.product.items[0].sellers[0].commertialOffer.Price
        } catch {
          price = 0
        }

        let categories : Categories = getCategories(categoryTree);
        currentPageData = {
          pageType: "prod",
          productPrice: price,
          productId: productId,
          productSku: selectedSku.itemId,
          productName,
          productUrl: window.location.origin + window.location.pathname,
          categoryName: categories.categoryNames,
          categoryId: categories.categoryIds,
          cartAction: "repeat"
        }

        reinitialize(currentPageData);
        toTrack = false;
      }
      return
    }
    case 'vtex:addToCart': {
      const { items } = e.data;

      if (items && items.length) {
        const data : FanplayrData = {
          ...currentPageData,
          cartAction: "add",
          products: items.map(item => ({
            id: item.productId,
            sku: item.skuId,
            price: item.price / 100,
            qty: item.quantity,
            name: item.name
          })),
          currency: e.data.currency,
        }

        reinitialize(data);
      }

      return
    }
    case 'vtex:cartChanged': {
      const { items } = e.data;

      if (items) {

        const data : FanplayrData = {
          ...currentPageData,
          cartAction: "set",
          products: items.map(item => ({
            id: item.productId,
            sku: item.skuId,
            price: item.price / 100,
            qty: item.quantity,
            name: item.name
          })),
          currency: e.data.currency,
        }

        reinitialize(data);
      }

      return
    }
    case 'vtex:orderPlaced': {
      const order = e.data
      window.fp_sales_orders = {
        version: 3,
        accountKey: window.fanplayrBaseTracking.accountKey,
        storeDomain: '',
        data: {
          orderId: order.transactionId,
          orderNumber: order.transactionId,
          gross: order.transactionSubtotal,
          total: order.transactionTotal,
          discount: order.transactionDiscounts,
          discountCode: order.coupon,
          shipping: order.transactionShipping,
          tax: order.transactionTax,
          currency: order.currency,
          products: order.transactionProducts.map(prod => {
            return {
              id: prod.id,
              sku: prod.sku,
              name: prod.name,
              price: prod.price,
              qty: prod.quantity,
              catId: prod.categoryIdTree.join(","),
              catName: prod.categoryTree.join(",")
            }
          }),
          cartAction: 'override'
        }
      };
      toTrack = false;
      return
    }
    case 'vtex:departmentView':
    case 'vtex:categoryView': {
      if (toTrack) {
        let category : string = '';
        let categoryId : string = '';
        if (e.data.products && e.data.products.length) {
          category = getCategory(e.data.products[0].categories) || '';
          categoryId = e.data.products[0].categoryId;
        }
        currentPageData = {
          pageType: "cat",
          categoryName: category,
          categoryId: categoryId || category,
          cartAction: "repeat"
        }

        reinitialize(currentPageData);
        toTrack = false;
      }
      return
    }
    case 'vtex:pageInfo': {
      if (toTrack) {
        if (e.data.eventType === "homeView") {
          currentPageData = {
            pageType: "home",
            cartAction: "repeat"
          }
          reinitialize(currentPageData);
          toTrack = false;
        }
      }
    }
    default: {
      return
    }
  }
}

export function cdpHandleEvents(e: PixelMessage) {
  switch (e.data.eventName) {
    case 'vtex:pageInfo': {
      if (e.data.eventType === "homeView") {
        window.fp360.page({
          type: "home"
        });
      }
      return
    }
    case 'vtex:productView': {
      const { selectedSku, productName, productId } = e.data.product
      let price = 0;
      try {
        price = e.data.product.items[0].sellers[0].commertialOffer.Price
      } catch {
        price = 0
      }

      window.fp360.track("Product Viewed", {
        $product: {
          id: productId,
          sku: selectedSku.itemId,
          name: productName,
          price: price,
          url: window.location.origin + window.location.pathname
        }
      })
      return
    }
    case 'vtex:addToCart': {
      const { items } = e.data;

      const item = Array.isArray(items) ? items[0] : items

      window.fp360.track("Add To Cart", {
        $product: {
          id: item.productId,
          sku: item.skuId,
          price: item.price,
          quantity: item.quantity,
          name: item.name
        }
      })

      return
    }
    case 'vtex:orderPlaced': {
      const order = e.data

      window.fp360.track("Order Completed", {
        $order: {
          orderNumber: order.transactionId,
          revenue: order.transactionSubtotal,
          discount: order.transactionDiscounts,
          discountCode: order.coupon,
          shipping: order.transactionShipping,
          tax: order.transactionTax,
          currency: order.currency,
          products: order.transactionProducts.map(prod => {
            return {
              id: prod.id,
              sku: prod.sku,
              name: prod.name,
              price: prod.price,
              quantity: prod.quantity
            }
          })
        }
      });

      window.fp_sales_orders = {
        version: 3,
        accountKey: window.fanplayrBaseTracking.accountKey,
        storeDomain: '',
        data: {
          orderId: order.transactionId,
          orderNumber: order.transactionId,
          gross: order.transactionSubtotal,
          total: order.transactionTotal,
          discount: order.transactionDiscounts,
          discountCode: order.coupon,
          shipping: order.transactionShipping,
          tax: order.transactionTax,
          currency: order.currency,
          products: order.transactionProducts.map(prod => {
            return {
              id: prod.id,
              sku: prod.sku,
              name: prod.name,
              price: prod.price,
              qty: prod.quantity,
              catId: prod.categoryIdTree.join(","),
              catName: prod.categoryTree.join(",")
            }
          }),
          cartAction: 'override'
        }
      };
      return
    }
    case 'vtex:pageView': {
      window.fp360.page({
        type: "page"
      });
      return
    }
    default: {
      return
    }
  }
}

function getCategories(categoriesTree : Category[]) : Categories {
  return {
    categoryIds: categoriesTree.map(c => c.id).join(","),
    categoryNames: categoriesTree.map(c => c.name).join(","),
  }
}

function getCategory(rawCategories: string[]) {
  if (!rawCategories || !rawCategories.length) {
    return
  }

  return removeStartAndEndSlash(rawCategories[0]);
}

// Transform this: "/Apparel & Accessories/Clothing/Tops/"
// To this: "Apparel & Accessories/Clothing/Tops"
function removeStartAndEndSlash(category: string) {
  return category && category.replace(/^\/|\/$/g, '')
}

if (canUseDOM) {
  if (window.enableFanplayrTargeting) {
    window.addEventListener('message', targetingHandleEvents)
  }
  if (window.enableFanplayr360) {
    window.addEventListener('message', cdpHandleEvents)
  }
}
