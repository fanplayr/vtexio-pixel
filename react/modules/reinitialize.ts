window.fanplayrTrackingCache = window.fanplayrTrackingCache || [];

export default function reinitialize(data: any) {
  if (window.fanplayrIsReady) {
    window.fanplayr.reinitialize({
      ...window.fanplayrBaseTracking,
      data
    });
  }
  else {
    window.fanplayrTrackingCache.push(data);
  }
}
