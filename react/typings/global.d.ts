interface Window extends Window {
  fanplayr: any,
  fp360: any,
  enableFanplayrTargeting: boolean,
  enableFanplayr360: boolean,
  fanplayrIsReady: boolean,
  fanplayrTrackingCache: any[],
  fanplayrOrderCache: boolean,
  fanplayrBaseTracking: any,
  fp_sales_orders: any
}
