export interface FanplayrTracking {
  version: number,
  accountKey: string,
  connectUrl: string,
  applyToCartUrl: string,
  sessionOfferUrl: string,
  data: FanplayrData,
  custom_data: object
}

export interface FanplayrData {
  lineItemCount?: number,
  numItems?: number,
  gross?: number,
  total?: number,
  discount?: number,
  discountCode?: string,
  pageType?: string,
  categoryId?: string,
  categoryName?: string,
  productId?: string,
  productName?: string,
  productSku?: string,
  productPrice?: number,
  productUrl?: string,
  productImage?: string,
  currency?: string,
  products?: FanplayrProduct[],
  cartAction?: FanplayrCartAction
}

export interface FanplayrProduct {
  id: string,
  sku: string,
  price: number,
  qty: number,
  name: string
}

export type FanplayrCartAction = "override" | "repeat" | "add" | "substract" | "set"
export type FanplayrPageType = "page" | "home" | "prod" | "cat" | "cart" | "checkout"
