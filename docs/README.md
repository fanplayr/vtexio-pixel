# Fanplayr

Open the VTEX App Store and install the app on your store.

or

Run the following command:

```sh
vtex install fanplayr.fanplayr-pixel@1.x
```

Fill in your Fanplayr's Account Key and click save.

To start tracking also the checkout pages, please follow the documentation at this link:

https://fanplayr.gitbook.io/general/integrations/vtex

To activate the service worker to use Web Push Notification, eneble it and deactivate the native service worker follow this guide:

https://vtex.io/docs/recipes/store-management/deactivating-the-vtex-io-native-service-worker/
